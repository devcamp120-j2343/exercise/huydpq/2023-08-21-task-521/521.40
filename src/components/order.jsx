import { Typography, Card, CardContent, CardActions, Button } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import { buyClick } from "../actions/order.action"
// import { Button } from "bootstrap"

const Order = () => {
    const { prices, mobileList } = useSelector((reduxData) => {
        return reduxData.orderReducer
    })
    const dispatch = useDispatch()

    const onBtnBuyClick = (index) => {
        dispatch(buyClick(index))
        
    }

    return (
        <div className="container mt-4">
            <Typography variant="h6">
                Mobile App
            </Typography>
            <div className="mt-4" style={{ display: "flex", justifyContent: "space-around" }}>
                {mobileList.map((elemet, index) => {
                    return (
                        <Card key={index} className="p-3" sx={{ width: 380 }}>
                            <CardContent>
                                <Typography sx={{ fontSize: 14, mb: 2 }} variant="body1" gutterBottom>
                                    <b>{elemet.name}</b>
                                </Typography>

                                <Typography sx={{ mb: 2 }} variant="body2" color="text.secondary">
                                    Price: {elemet.price} USD
                                </Typography>
                                <Typography variant="body2">
                                    Quantity: {elemet.quantity}
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button onClick={() => onBtnBuyClick(index)} variant="contained" color="success">Buy</Button>
                            </CardActions>
                        </Card>
                    )
                })}
            </div>
            <div>
                <Typography sx={{ fontSize: 14, mt: 2 }} variant="body1" gutterBottom>
                    <b>Total: {prices} USD</b>
                </Typography>
            </div>
        </div>
    )
}

export default Order