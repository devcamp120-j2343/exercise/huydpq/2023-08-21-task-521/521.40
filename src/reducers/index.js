import { combineReducers } from "redux"
import orderReducer from "./orderReducer"

const rootRedeucer = combineReducers({
    orderReducer
})

export default rootRedeucer
